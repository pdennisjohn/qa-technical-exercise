
The test automation  browser tests on Confluence cloud were carried out in the following test environment.

Pre-Requisities
---------------
System Specs : CPU Intel Core i5-2430M2.4GHz
Operating System : Windows 7 ultimate (Special Package 2)
Web Browsers:Mozilla FireFox Version 40.0.3
Java Version : Version 8 Update 60
Automation Tools : Selenium Webdriver 2.0 Version : 2.47.0 , Eclipse Java EE IDE Version : 4.5.0



Assumptions
-----------

1. Estimated man hours for completion is 1 day.
2. The automated tests to be automated and verfied in multi-browser platforms .i.e Google Chrome, Microsoft Internet Explorer and Mozilla FireFox.
3. The scope of the tests are user can create new page in Confluence and User can set page restrictions.
4. The test code will be fully tested and result will be submitted on or before 04/09/2015.


Issues
------

The test code fails to run in Google Chrome and Microsoft Internet Browsers because of different Java Versions in the browsers.
 